"use strict";

/////////////////////////////////////////////////////////////////////////////////////////
//
// cs411 assignment 1 (Fall 2017) - raster graphics
//
/////////////////////////////////////////////////////////////////////////////////////////

var ctx;
var imageData;

var pauseFlag=1;
var lineFlag=1;
var triangleFlag=1;
var fillFlag=1;
var uniformFlag=1;

function togglePause() {
  pauseFlag= 1- pauseFlag;
  console.log('pauseFlag = %d', pauseFlag);
}

function toggleLine() {
  lineFlag= 1- lineFlag;
  console.log('lineFlag = %d', lineFlag);
}

function toggleTriangle() {
  triangleFlag= 1- triangleFlag;
  console.log('triangleFlag = %d', triangleFlag);
}

function toggleFill() {
  fillFlag= 1- fillFlag;
  console.log('fillFlag = %d', fillFlag);
}

function toggleUniform() {
  uniformFlag= 1- uniformFlag;
  console.log('uniformFlag = %d', uniformFlag);
}

//var once = true;

function animate() 
{
  if(!pauseFlag) {
    if (lineFlag) drawRandomLineSegment();
    if (triangleFlag) drawRandomTriangle();
  }
  setTimeout(animate,100); // call animate() in 1000 msec

 //  if (imageData && once) {
 //  	var data = imageData.data;
	// drawLineSegment([300, 300], [50, 0], [255, 0, 0])
	// once = false;
 //  }

} 


function initImage(img) 
{
  var canvas = document.getElementById('mycanvas');
  ctx = canvas.getContext('2d');

  ctx.drawImage(img, 0, 0);
  imageData = ctx.getImageData(0,0,canvas.width, canvas.height); // get reference to image data
}


function main()
{
  // load and display image
  var img = new Image();
  img.src = 'data/frac2.png';
  img.onload = function() { initImage(this);}

  // set button listeners
  var grayscalebtn = document.getElementById('grayscaleButton');
  grayscalebtn.addEventListener('click', grayscale);

  var pausebtn = document.getElementById('pauseButton');
  pausebtn.addEventListener('click', togglePause);

  var linebtn = document.getElementById('lineButton');
  linebtn.addEventListener('click', toggleLine);

  var trianglebtn = document.getElementById('triangleButton');
  trianglebtn.addEventListener('click', toggleTriangle);

  var fillbtn = document.getElementById('fillButton');
  fillbtn.addEventListener('click', toggleFill);

  var uniformbtn = document.getElementById('uniformButton');
  uniformbtn.addEventListener('click', toggleUniform);

  // start animation
  animate();
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// conversion to grayscale
// 
/////////////////////////////////////////////////////////////////////////////////////////

function grayscale() 
{
  var data = imageData.data;
  for (var i = 0; i < data.length; i += 4) {
    var m = (data[i] + data[i +1] + data[i +2]) / 3;
    data[i]     = m; // red
    data[i + 1] = m; // green
    data[i + 2] = m; // blue
  }
  ctx.putImageData(imageData, 0, 0);
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// draw lines
//
/////////////////////////////////////////////////////////////////////////////////////////

function colorPoint(data, x, y, w, h, color) {
	var yi=h-y;//invert y coordinate
	data[(yi*w+x)*4+0]     = color[0]; // red
	data[(yi*w+x)*4+1]     = color[1]; // green
	data[(yi*w+x)*4+2]     = color[2]; // blue
	return data;
}

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// REPLACE THIS WITH YOUR FUNCTION FOLLOWING THE ASSIGNMENT SPECIFICATIONS
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
function drawLineSegment(v1,v2,color)
{

	var data = imageData.data;
	var h = imageData.height;
	var w = imageData.width;

	// Arrange vertices by x coordinate to avoid Math.min()/Math.max() calls later
	// By default, assume nearly horizontal line, and arrange from min x to max x
	var vs = v1[0] < v2[0] ? v1 : v2;
	var ve = v1[0] < v2[0] ? v2 : v1;
	
	// If the line changes more horizontally than it does vertically, it is considered nearly horizontal
	var dx = ve[0] - vs[0];
	var dy = ve[1] - vs[1];
	var isNearlyHorizontal = (Math.abs(dx) >= Math.abs(dy));

	// If the assumption about the line being nearly horizontal is false, arrange the vertices from min y to max y instead
	if (!isNearlyHorizontal) {
		vs = v1[1] < v2[1] ? v1 : v2;
		ve = v1[1] < v2[1] ? v2 : v1;
		dx = ve[0] - vs[0];
		dy = ve[1] - vs[1];
	}

	// ignore illegal lines
	if ((vs[0] <0) || (vs[1] <0) || (ve[0] >= w) || (ve[1] >= h)) return;
	if ((vs[0] == ve[0]) && (vs[1] == ve[1])) return;

	// handle nearly horizontal lines
	if(isNearlyHorizontal){
		var e_bar = Math.abs(dy) - Math.abs(dx);
		var y = vs[1];
		// Determine what direction to increment y in
		var direction = dy / Math.abs(dy);
		for (var x = vs[0]; x <= ve[0]; x++) {
			data = colorPoint(data, x, y, w, h, color);
			// Update integer error and increment in appropriate direction if needed
			if (e_bar > 0) {
				y += direction;
				e_bar -= Math.abs(dx);
			}
			e_bar += Math.abs(dy);
		}    
	}

	// handle nearly vertical lines
	else {
		var e_bar = Math.abs(dx) - Math.abs(dy);
		var x = vs[0];
		// Determine what direction to increment x in
		var direction = dx / Math.abs(dx);
		for (var y = vs[1]; y <= ve[1]; y++) {
			data = colorPoint(data, x, y, w, h, color);
			// Update integer error and increment in appropriate direction if needed
			if (e_bar > 0) {
				x += direction;
				e_bar -= Math.abs(dy);
			}
			e_bar += Math.abs(dx);
		}    
	}
	// update image
	ctx.putImageData(imageData, 0, 0);
}


function drawRandomLineSegment()
{
  var h = imageData.height;
  var w = imageData.width;
  
  var xs=Math.floor(Math.random()*w);
  var ys=Math.floor(Math.random()*h);
  var xe=Math.floor(Math.random()*w);
  var ye=Math.floor(Math.random()*h);
  var r=Math.floor(Math.random()*255);
  var g=Math.floor(Math.random()*255);
  var b=Math.floor(Math.random()*255);

  drawLineSegment([xs,ys] ,[xe,ye],[r,g,b]);
}


/////////////////////////////////////////////////////////////////////////////////////////
//
// draw triangles
//
/////////////////////////////////////////////////////////////////////////////////////////

function invertColor(color) {
	return [
		255 - color[0],
		255 - color[1],
		255 - color[2]
	];
}

function getSlope(v1, v2) {
	var dy = v2[1] - v1[1];
	var dx = v2[0] - v1[0];
	return dy / dx;
}

function triangleArea(a,b,c)
{
  var area = ((b[1] - c[1]) * (a[0] - c[0]) + (c[0] - b[0]) * (a[1] - c[1]));
  area = Math.abs(0.5*area);
  return area;
}

function vertexInside(v,v0,v1,v2)
{
  var T = triangleArea(v0,v1,v2);

  var alpha = triangleArea(v,v0,v1) /T ;
  var beta  = triangleArea(v,v1,v2) /T ;
  var gamma = triangleArea(v,v2,v0) /T ;

  if ((alpha>=0) && (beta>=0) && (gamma>=0) && (Math.abs(alpha+beta+gamma -1)<0.00001)) return true;
  else return false;
}


//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// REPLACE THIS WITH YOUR FUNCTION FOLLOWING THE ASSIGNMENT SPECIFICATIONS
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
var tri = false;
function drawTriangle(v0,v1,v2,color)
{

	if (tri)
		//return;
	tri = true;


	var data = imageData.data;
	var h = imageData.height;
	var w = imageData.width;

	if ((v0[0] == v1[0]) && (v0[1] == v1[1])) return;
	if ((v1[0] == v2[0]) && (v1[1] == v2[1])) return;
	if ((v2[0] == v0[0]) && (v2[1] == v0[1])) return;

	if (!fillFlag){
		drawLineSegment(v0 ,v1, color);
		drawLineSegment(v1 ,v2, color);
		drawLineSegment(v2 ,v0, color);
	}

	// handle filled triangles
	else
	{

		var xmin = Math.min(v0[0],v1[0],v2[0]);
		var xmax = Math.max(v0[0],v1[0],v2[0]);
		var ymin = Math.min(v0[1],v1[1],v2[1]);
		var ymax = Math.max(v0[1],v1[1],v2[1]);


		// Sort points by y coordinate: since we use horizontal scanlines, we iterate over y pixels
		var p = [v0, v1, v2].sort((a, b) => {
			return a[1] - b[1];
		});

		// Invert color to use for non-uniform lines
		var original = color;
		var inverted = invertColor(color);
		var alternator = true;

		// Fill bottom half of triangle
		var m1 = getSlope(p[0], p[1]);
		var m2 = getSlope(p[0], p[2]);
		var x1 = p[0][0];
		var x2 = p[0][0];
		var x1_real = x1;
		var x2_real = x2;
		for (var y = p[0][1]; y <= p[1][1]; y++) {
			if (!uniformFlag) {
				color = alternator ? original : inverted;
				alternator = !alternator;
			}
			drawLineSegment([x1, y], [x2, y], color);
			var dx1 = (1 / m1);
			var dx2 = (1 / m2);
			// Main floating-point values to reduce error over course of drawing
			x1_real += dx1;
			x2_real += dx2;
			x1 = Math.round(x1_real);
			x2 = Math.round(x2_real);
		}

		// Fill top half of trianngle
		m1 = getSlope(p[2], p[0]);
		m2 = getSlope(p[2], p[1]);
		x1 = p[2][0];
		x2 = p[2][0];
		x1_real = x1;
		x2_real = x2;
		for (var y = p[2][1]; y >= p[1][1]; y--) {
			if (!uniformFlag) {
				color = alternator ? original : inverted;
				alternator = !alternator;
			}
			drawLineSegment([x1, y], [x2, y], color);
			var dx1 = (1 / m1);
			var dx2 = (1 / m2);
			// Main floating-point values to reduce error over course of drawing
			x1_real -= dx1;
			x2_real -= dx2;
			x1 = Math.round(x1_real);
			x2 = Math.round(x2_real);
		}

	}

	ctx.putImageData(imageData, 0, 0);

}


function drawRandomTriangle()
{
  var h = imageData.height;
  var w = imageData.width;
  
  var v0x=Math.floor(Math.random()*w);
  var v0y=Math.floor(Math.random()*h);
  var v1x=Math.floor(Math.random()*w);
  var v1y=Math.floor(Math.random()*h);
  var v2x=Math.floor(Math.random()*w);
  var v2y=Math.floor(Math.random()*h);
  var r=Math.floor(Math.random()*255);
  var g=Math.floor(Math.random()*255);
  var b=Math.floor(Math.random()*255);

  drawTriangle([v0x,v0y], [v1x,v1y], [v2x,v2y], [r,g,b]);

}



//
// EOF
//
