let out = document.getElementById('hw');

function showAnswer(opt) {
	let div = document.createElement('div');
	div.innerHTML = `
		<h4>${opt.question}</h4>
		<p>${opt.answer}</p>
	`;
	out.appendChild(div);
}

// 2a

let v1 = new Vector4([1, 1, 1, 1]);
let t1 = new Matrix4().setTranslate(2, 3, 0);
let a = t1.multiplyVector4(v1);

console.log(v1.str())

showAnswer({
	question: `2a) Given the 2D point (1, 1) find its coordinates after translating it by (2, 3).`,
	answer: `The result is (${a.elements[0]}, ${a.elements[1]}).`
});

// 2b
	
let s1 = new Matrix4().setScale(2, 2, 1);
let b = s1.multiplyVector4(v1);

showAnswer({
	question: `2b) Given the 2D point (1, 1) find its coordinates after scaling it by (2, 2).`,
	answer: `The result is (${b.elements[0]}, ${b.elements[1]}).`
});

// 2c

let r1 = new Matrix4().setRotate(45, 0, 0, 1);
let c = r1.multiplyVector4(v1);

showAnswer({
	question: `2c) Given the 2D point (1, 1) find its coordinates after rotating it by 45 degrees.`,
	answer: `The result is (${c.elements[0]}, ${c.elements[1]}).`
});

// 2d

showAnswer({
	question: `2d) Given the 2D point (1, 1) find its coordinates in homogenous coordinates.`,
	answer: `The result is (${v1.elements[0]}, ${v1.elements[1]}, ${v1.elements[2]}).`
});

// 2e

let v2 = new Vector4([1, 1, 2, 1]);
let homoScale = v2.elements[2];
let s2 = new Matrix4().setScale(1 / homoScale, 1 / homoScale, 1 / homoScale);
let h1 = s2.multiplyVector4(v2);

showAnswer({
	question: `2e) Given the 2DH point (1, 1, 2) in homogenous coordinates, find the 2D point corresponding to it.`,
	answer: `The result is (${h1.elements[0]}, ${h1.elements[1]}).`
});

// 2f

let v3 = new Vector4([1, 2, 3, 1]);
let s3 = new Matrix4().setScale(2, 2, 2);
let h2 = s3.multiplyVector4(v3);

showAnswer({
	question: `2f) Given the 2DH point (1, 2, 3) in homogenous coordinates, find another homogenous point representing the same 2D point.`,
	answer: `The result is (${h2.elements[0]}, ${h2.elements[1]}, ${h2.elements[2]}).`
});

// 2g

let v4 = new Vector4([1, 2, 3, 1]);
let homoScale2 = v4.elements[2];
let s4 = new Matrix4().setScale(1 / homoScale2, 1 / homoScale2, 1 / homoScale2);
let h3 = s4.multiplyVector4(v4);

showAnswer({
	question: `2g) Given the 2DH point (1, 2, 3) in homogenous coordinates, find the (x, y) coordinates of the 2D point corresponding to it.`,
	answer: `The result is (${h3.elements[0]}, ${h3.elements[1]}).`
});

// 2i

let v5 = new Vector4([2, 5, 1, 1]);
let r3 = new Matrix4().setRotate(30, 0, 0, 1);
let i = r3.multiplyVector4(v5);

showAnswer({
	question: `2i) Given the 2D point (2, 5) find its coordinate after rotating it by 30 degrees about the origin.`,
	answer: `The result is (${i.elements[0]}, ${i.elements[1]}).`
});

// 2j

let r4t1 = new Matrix4().setTranslate(-1, -2, 0);
let r4 = new Matrix4().setRotate(30, 0, 0, 1);
let r4t2 = new Matrix4().setTranslate(1, 2, 0);
let j = r4t2.multiplyVector4(r4.multiplyVector4(r4t1.multiplyVector4(v5)));

showAnswer({
	question: `2j) Given the 2D point (2, 5) find its coordinate after rotating it by 30 degrees about the the point (1, 2).`,
	answer: `The result is (${j.elements[0]}, ${j.elements[1]}).`
});

// 2k

let r5 = new Matrix4().setRotate(45, 0, 0, 1);
let t5 = new Matrix4().setTranslate(3, 4, 0);
let k = r5.multiplyVector4(t5.multiplyVector4(v5));

showAnswer({
	question: `2k) Given the 2D point (2, 5) find its coordinate after translating it by (3, 4) and then rotating it by 45 degrees about the origin.`,
	answer: `The result is (${k.elements[0]}, ${k.elements[1]}).`
});

// 2l

let l = t5.multiplyVector4(r5.multiplyVector4(v5));

showAnswer({
	question: `2l) Given the 2D point (2, 5) find its coordinate after rotating it by 45 degrees about the origin and then translating it by (3, 4).`,
	answer: `The result is (${l.elements[0]}, ${l.elements[1]}).`
});

// 2m

function worldToCamera1(v_world) {
	let t1_world = new Matrix4().setTranslate(1, 2, 0).invert();
	let r1_world = new Matrix4().setRotate(45, 0, 0, 1).invert();
	return r1_world.multiplyVector4(t1_world.multiplyVector4(v_world));
}

let v6 = new Vector4([5, 6, 1, 1]);
let m = worldToCamera1(v6);

showAnswer({
	question: `2m) Given the point (5, 6) in a world coordinate systems, find its coordinates in a camera coordinate system. Assume that the camera coordinates system is translated by (1, 2) with respect to the world coordinate system and rotated by 45 degrees with respect to the world coordinate system.`,
	answer: `The result is (${m.elements[0]}, ${m.elements[1]}).`
});








